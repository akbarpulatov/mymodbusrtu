#ifndef __AKBARFUNCTIONS_H
#define __AKBARFUNCTIONS_H

//============================== < DEFINES OF FUNCTIONS.C > ===========================================================
void myInit(void);		//Function which is called from main.c before loop for initializing
void myMainLoop(void); 	//Loop which is called from Main loop of main.c

//============================== < VARIABLES TO FUNCTIONS.C > ===========================================================


//--------------------------------- < TIMERS > --------------------------------------------------------------------------

//--------------------------------- < Flags > ---------------------------------------------------------------------------

//--------------------------------- < EXTERNAL VARIABLES TO FUNCTIONS.C > -----------------------------------------------


//--------------------------------- < INTERNAL VARIABLES TO FUNCTIONS.C > -----------------------------------------------


//============================== < FUNCTION PROTOTYPES OF FUNCTIONS.C > ==================================================
//---------------------------------- < HIGH LEVEL FUNCTIONS > ------------------------------------------------------------


//---------------------------------- < MID LEVEL FUNCTIONS > -------------------------------------------------------------


//---------------------------------- < LOW LEVEL FUNCTIONS > -------------------------------------------------------------

//======================================== < CONSTANT VECTORS > ==========================================================



#endif // !__AKBARFUNCTIONS_H
