#include "main.h"
#include "Functions.h"
#include "port.h"
#include "mbport.h"
#include "mb.h"

#define REG_INPUT_START 1000	// Adress boshlanishi,
#define REG_INPUT_NREGS 64		//virtualniy tasavvurdagi registrlar soni

#define REG_TIME_START		2000
#define REG_TIME_NREGS		6
#define REG_TEXT_START		3000
#define REG_TEXT_NREGS		64


UCHAR MBdatatemp[20];
UCHAR TimeDateRegister[REG_TIME_NREGS];
UCHAR TextRegister[REG_TEXT_NREGS];

static USHORT usRegInputStart = REG_INPUT_START;
static USHORT usRegInputBuf[REG_INPUT_NREGS];	

//============= < TOP LEVEL AND HARDWARE CALLBACK FUNCTIONS > ====================================================================
inline void myInit()		//Function which is called from main.c before loop for initializing
{
	eMBErrorCode    eStatus;

	eStatus = eMBInit(MB_RTU, 0x11, 1, 38400, MB_PAR_NONE);

	/* Enable the Modbus Protocol Stack. */
	eStatus = eMBEnable();
}

inline void myMainLoop() 	//Loop which is called from Main loop of main.c
{
	for (USHORT i = 0; i < REG_TEXT_NREGS; i++)
	{
		usRegInputBuf[i] = TextRegister[i];
	}
	
	(void)eMBPoll();
	
}

void HAL_IncTick(void)
{

}

//void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)		// Interrupts from Timers 
//{
//
//}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)	//External Interrupt Function
{

}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) // UART received data
{
	

	
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	
}



//====================================== < HIGH LEVEL FUNCTIONS > ================================================================

//====================================== < MID LEVEL FUNCTIONS > =================================================================

//0x04		Read AI / Read Input Registers
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs)
{
	usAddress--;      // o`zim qo`shgan qo`shimcha, bibliotekani xatosi bo`lsa kerak.
	
	eMBErrorCode    eStatus = MB_ENOERR;
	int             iRegIndex;

	if ((usAddress >= REG_INPUT_START)
	    && (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS))
	{
		iRegIndex = (int)(usAddress - usRegInputStart);
		while (usNRegs > 0)
		{
			*pucRegBuffer++ =
			    (unsigned char)(usRegInputBuf[iRegIndex] >> 8);
			*pucRegBuffer++ =
			    (unsigned char)(usRegInputBuf[iRegIndex] & 0xFF);
			iRegIndex++;
			usNRegs--;
		}
	}
	else
	{
		eStatus = MB_ENOREG;
	}

	return eStatus;
}
//0x03//0x06//0x10		Read AO / Read Holding Registers 
eMBErrorCode eMBRegHoldingCB(UCHAR* pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode)
{
	for (USHORT i = 0; i < usNRegs * 2; i++)
	{
		MBdatatemp[i] = pucRegBuffer[i];
	}
	
	return MB_ENOERR;
}
//0x01//0x05//0x0F		Read DO / Read Coil Status // shu komanda bilan o`qib olamiz kerakli xabar stringni 
eMBErrorCode eMBRegCoilsCB(UCHAR* pucRegBuffer, USHORT usAdress, USHORT usNCoils, eMBRegisterMode eMode)
{
	usAdress--;
	USHORT usNBytes = usNCoils / 8;
	eMBErrorCode    eStatus = MB_ENOERR;
	
	if (eMode == MB_REG_WRITE)
	{
		int iRegIndex;
		
		//Time address implementation
		if((usAdress >= REG_TIME_START) && (usAdress + usNBytes <= REG_TIME_START + REG_TIME_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_TIME_START);
			while (usNBytes-- > 0)
			{
				TimeDateRegister[iRegIndex++] = *pucRegBuffer++;
			}
		}
		//Text address implementation
		else if((usAdress >= REG_TEXT_START) && (usAdress + usNBytes <= REG_TEXT_START + REG_TEXT_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_TEXT_START);
			while (usNBytes-- > 0)
			{
				TextRegister[iRegIndex++] = *pucRegBuffer++;
			}
		}
		
		
		else
		{
			eStatus = MB_ENOREG;
		}
		
		return eStatus;
	}
	
}
//0x02 Read DI / Read Input Status
eMBErrorCode eMBRegDiscreteCB(UCHAR* pucRegBuffer, USHORT usAddress, USHORT usNDiscrete) 
{
	UCHAR DiscreteData[6];
	
	DiscreteData[0] = 0x00;
	DiscreteData[1] = 0x01;
	DiscreteData[2] = 0x02;
	DiscreteData[3] = 0x03;
	DiscreteData[4] = 0x04;
	DiscreteData[5] = 0x05;
	
	return MB_ENOERR;
}

//====================================== < LOW LEVEL FUNCTIONS > =================================================================	























